#include <Arduino.h>
#include <Hash.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WebSocketsServer.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>


#define WIFI_NAME "Fjare Network"
#define WIFI_PASS "newsoftheworld"

#define UP_PIN     13
#define DOWN_PIN   12
#define DIR_PIN    2
#define STEP_PIN   16
#define SLEEP_PIN  14
#define NEO_PIN    15

#define NUM_NEO_LEDS   2

#define BNO055_SAMPLERATE_DELAY_MS 100
#define MOTOR_STEP_WIDTH_US        500
#define MOTOR_DELAY_US             15000
#define DEBOUNCE_THRESHOLD_US      1000000
#define TEMP_CHECK_INTERVAL_MS     600000

#define CW   1
#define CCW  2

#define UP   1
#define DOWN 0

#define ANGLE_MAX_DEG         (64.0)
#define ANGLE_MIN_DEG         (-74.0)
#define ANGLE_OPEN_DEG        (-20.0)
#define ANGLE_CLOSED_DOWN_DEG (-72.0)
#define ANGLE_CLOSED_UP_DEG   (60.0)
#define ANGLE_THRESHOLD_DEG   (1.0)

#define TEMP_MAX_C 75

uint8_t interrupt_up = 0;
uint8_t interrupt_down = 0;

unsigned long last_micros_motor, last_micros_up, last_micros_down;
unsigned long last_millis_temp;

bool up_button_pressed = false, down_button_pressed = false;
bool motor_spinning = false, executing_command = false;
bool error = false;
bool error_sensor = false, error_angle = false, error_temp = false;

float desired_angle_deg = 0.0;

uint8_t motor_direction = 0;

sensors_event_t event;

Adafruit_BNO055 bno = Adafruit_BNO055(55);

ESP8266WiFiMulti WiFiMulti;
ESP8266WebServer server = ESP8266WebServer(80);
WebSocketsServer webSocket = WebSocketsServer(81);

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_NEO_LEDS, NEO_PIN, NEO_GRB + NEO_KHZ800);
// Define NeoPixel colors
uint32_t neo_color_error = strip.Color(255, 0, 0); // red
uint32_t neo_color_angle_error = strip.Color(255, 165, 0); // orange
uint32_t neo_color_temp_error = strip.Color(255, 255, 0); // yellow
uint32_t neo_color_motor = strip.Color(0, 0, 255); // blue
uint32_t neo_color_command = strip.Color(0, 255, 0); // green
uint32_t neo_color_off = strip.Color(0, 0, 0); // off


/* Interrupt Service Routines for UP and DOWN buttons*/
void upInterruptISR() {
  interrupt_up++;
}
void downInterruptISR() {
  interrupt_down++;
}

void setLED(uint32_t color) {
  for (uint32_t i=0; i<NUM_NEO_LEDS; i++) {
    strip.setPixelColor(i, color);
  }
  strip.show();
  delay(50);
}

void flashLED(uint32_t color, uint32_t flashes) {
  for (uint32_t j=0; j<flashes; j++) {
    for (uint32_t i=0; i<NUM_NEO_LEDS; i++) {
      strip.setPixelColor(i, color);
    }
    strip.show();
    delay(200);
    for (uint32_t i=0; i<NUM_NEO_LEDS; i++) {
      strip.setPixelColor(i, strip.Color(0, 0, 0));
    }
    strip.show();
    delay(300);
  }
}

void stopMotor() {
  digitalWrite(SLEEP_PIN, LOW);
  motor_spinning = false;
  executing_command = false;

  /* Set motor direction pin HIGH to keep blue LED on ESP8266 off */
  motor_direction = CCW;
  digitalWrite(DIR_PIN, HIGH);

  setLED(neo_color_off);
  Serial.println("Stop motor");
}

void startMotor(uint8_t direction) {
  motor_direction = direction;
  setLED(neo_color_motor);
  digitalWrite(SLEEP_PIN, HIGH);
  motor_spinning = true;
  // Wait 1 ms for charge pump on motor driver to stabilize
  delayMicroseconds(1000);
  Serial.println("Start motor");
}

void gotoAngle(float angle){
  if (!executing_command
     && angle <= ANGLE_CLOSED_UP_DEG && angle >= ANGLE_CLOSED_DOWN_DEG) {
    desired_angle_deg = angle;
    executing_command = true;
    flashLED(neo_color_command, 2);
  } else {
    flashLED(neo_color_error, 3);
    Serial.println("wtf");
  }
}

void checkError(sensors_event_t *event) {
  uint8_t system_status = 0;
  int8_t temp = 0;

  /* Check orientation sensor for malfunction */

  bno.getSystemStatus(&system_status);

  if (system_status != 5) {
    error_sensor = true;
  } else if (error_sensor) {
    error_sensor = false;
  }

  /* Check temperature of orientation sensor */
  if (millis() - last_millis_temp > TEMP_CHECK_INTERVAL_MS) {
    temp = bno.getTemp();
    if (temp > TEMP_MAX_C) {
      error_temp = true;
    } else if (error_temp) {
      error_temp = false;
    }
    last_millis_temp = millis();
  }

  /* Check if the angle of the orientation sensor is within bounds */
  if (event->orientation.z > ANGLE_MAX_DEG ||
      event->orientation.z < ANGLE_MIN_DEG) {
        error_angle = true;
  } else if (error_angle) {
    error_angle = false;
  }

  /* Update error boolean */
  if (error_sensor || error_angle || error_temp) {
    error = true;
    stopMotor();
  } else if (error) {
    error = false;
    setLED(neo_color_off);
  }

  /* Set Neopixel color to indicate error type */
  uint32_t error_color;
  if (error_temp) {
    error_color = neo_color_temp_error;
    Serial.printf("Error! Temperature is %d C.\n", temp);
  } if (error_angle) {
    error_color = neo_color_angle_error;
    Serial.printf("Error! Angle is %6.4f degrees.\n", event->orientation.z);
  } if (error_sensor) {
    error_color = neo_color_error;
    Serial.printf("Error! System status is %d.\n", system_status);
  } if (error) {
    setLED(error_color);
  }
}

void checkAngle(sensors_event_t *event, float *angle){

  if (motor_spinning && motor_direction == CW) {

    if( event->orientation.z > (*angle - ANGLE_THRESHOLD_DEG) ){
      stopMotor();
    }

  } else if (motor_spinning && motor_direction == CCW){

    if( event->orientation.z < (*angle + ANGLE_THRESHOLD_DEG) ){
      stopMotor();
    }

  } else {

    if(event->orientation.z < (*angle - ANGLE_THRESHOLD_DEG)){
      startMotor(CW);
    } else if (event->orientation.z > (*angle + ANGLE_THRESHOLD_DEG) ) {
      startMotor(CCW);
    }

  }

}

void onInterrupt(uint8_t direction, unsigned long *last_micros,
                 bool *button_pressed){

  if (micros() - *last_micros < DEBOUNCE_THRESHOLD_US) {
    // Ignore interrupt
    *last_micros = micros();
  } else {
      if (direction == UP) {
        gotoAngle(ANGLE_OPEN_DEG);
      } else if (direction == DOWN) {
        gotoAngle(ANGLE_CLOSED_DOWN_DEG);
      }
      *last_micros = micros();
  }
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload,
                    size_t lenght) {

    switch(type) {
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            break;
        case WStype_CONNECTED: {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n",
                          num, ip[0], ip[1], ip[2], ip[3], payload);
        }
            break;
        case WStype_TEXT:
            Serial.printf("[%u] get Text: %s\n", num, payload);
            if(payload[0] == '#') {
              int32_t cmdint = (int32_t) strtol((const char *) &payload[1],
                                 NULL, 10);
              gotoAngle((float) cmdint);
            }
            break;
    }

}

void setup() {
  Serial.begin(115200);

  /* Initialize motor driver pins */
  pinMode(SLEEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);

  /* Put motor driver to sleep */
  digitalWrite(SLEEP_PIN, LOW);

  /* Set direction pin HIGH to keep blue light on ESP8266 off */
  digitalWrite(DIR_PIN, HIGH);

  /* Initialize NeoPixels */
  strip.begin();
  strip.show();

  /* Attach button interupts */
  attachInterrupt(digitalPinToInterrupt(UP_PIN), upInterruptISR, FALLING);
  attachInterrupt(digitalPinToInterrupt(DOWN_PIN), downInterruptISR, FALLING);

  /* Initialise the orientation sensor */
  if(!bno.begin())
  {
  /* There was a problem detecting the BNO055 ... check your connections */
  Serial.print("No BNO055 detected");
  while(1);
  }
  delay(1000);
  bno.setExtCrystalUse(true);

  Serial.println();
  Serial.println();
  Serial.println();

  for(uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFiMulti.addAP(WIFI_NAME, WIFI_PASS);

  while(WiFiMulti.run() != WL_CONNECTED) {
    delay(100);
    Serial.println("");
    Serial.println("WiFi connected");
  }

  /* Start WebSocket server */
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);

  if(MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", []() {
    server.send(200, "text/html",
      "<html class=\"no-js\" lang=\"\">"
      "<head>"
        "<meta charset=\"utf-8\">"
        "<meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">"
        "<title>Blinds</title>"
        "<meta name=\"description\" content="">"
        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
      "</head>"
      "<body>"
        "<p>WiFi Blinds</p>"
      "</body>"
      "</html>"
    );
  });
  server.begin();

  /* Add service to MDNS */
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("ws", "tcp", 81);

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {

  webSocket.loop();
  server.handleClient();

  /* Update orientation sensor values */
  bno.getEvent(&event);

  /* Determine if an interrupt was triggered by UP Button */
  if (interrupt_up > 0) {
    interrupt_up--;
    onInterrupt(UP, &last_micros_up, &up_button_pressed);
  }

  /* Determine if an interrupt was triggered by DOWN Button */
  if (interrupt_down > 0) {
    interrupt_down--;
    onInterrupt(DOWN,  &last_micros_down, &down_button_pressed);
  }

  /* Monitor angle of blinds and decide if motor should take a step */

  if (executing_command) {
    checkAngle(&event, &desired_angle_deg);
  }

  /* Make sure orientation sensor is fully operational before motor is
     allowed to move */
  checkError(&event);

  /* Tell stepper motor to take a single step */
  if( motor_spinning && !error
      && (micros() - last_micros_motor) > MOTOR_STEP_WIDTH_US) {

    int dir = (motor_direction == CW)? LOW:HIGH;

    // Set direction
    digitalWrite(DIR_PIN, dir);

    // Take a step
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(MOTOR_STEP_WIDTH_US);
    digitalWrite(STEP_PIN, LOW);

    delayMicroseconds(MOTOR_DELAY_US);
    last_micros_motor = micros();

  }

}
